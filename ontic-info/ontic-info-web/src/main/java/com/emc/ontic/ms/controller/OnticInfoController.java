package com.emc.ontic.ms.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.emc.ontic.ms.commons.constants.ControllerActions;
import com.emc.ontic.ms.commons.domain.ModifyDeliveryConditionsRequest;
import com.emc.ontic.ms.commons.domain.ModifySubscriptionRequest;
import com.emc.ontic.ms.commons.domain.StartSubscriptionRequest;
import com.emc.ontic.ms.commons.domain.StopDeliveryConditionsRequest;
import com.emc.ontic.ms.commons.domain.StopSubscriptionRequest;
import com.emc.ontic.ms.commons.rabbit.OnticMessageChannelAdapter;
import com.emc.ontic.ms.commons.rabbit.ReceivedMessage;
import com.emc.ontic.ms.commons.rabbit.SentMessage;
import com.emc.ontic.ms.service.OnticInfoService;

@RestController
public class OnticInfoController {
	
	private static final Logger logger = LoggerFactory.getLogger(OnticInfoController.class);
	
	@Autowired
	private OnticInfoService onticInfoService;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	private OnticMessageChannelAdapter rabbitAdapter;
	
    @RequestMapping(value = ControllerActions.START_SUBSCRIPTION_ACTION, method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public void startSubscription(@RequestBody StartSubscriptionRequest request) throws Exception {
    	logger.info("OnticInfoController::startSubscription Start");
    	
    	logger.info("StartSubscriptionRequest values {}", request);
    	
    	rabbitAdapter.sendReceivedMessage(new ReceivedMessage(request.toString()));
    	
    	onticInfoService.startSubscription(request);
    	
    	logger.info("OnticInfoController::startSubscription End");
    	
    	rabbitAdapter.sendSentMessage(new SentMessage(HttpStatus.CREATED.toString()));
    }
    
    @RequestMapping(value = ControllerActions.MODIFY_SUBSCRIPTION_ACTION, method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void modifySubscription(@PathVariable String subsid, @RequestBody ModifySubscriptionRequest request) {
    	logger.info("OnticInfoController::modifySubscription Start");
    	
    	request.setSubsID(subsid);
    	
    	logger.info("ModifySubscriptionRequest values {}", request);
    	
    	rabbitAdapter.sendReceivedMessage(new ReceivedMessage(request.toString()));
    	
    	onticInfoService.modifySubscription(request);
    	
    	logger.info("OnticInfoController::modifySubscription End");
    	
    	rabbitAdapter.sendSentMessage(new SentMessage(HttpStatus.OK.toString()));
    }
    
    @RequestMapping(value = ControllerActions.STOP_SUBSCRIPTION_ACTION, method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void stopSubscription(@PathVariable String subsid, @RequestBody StopSubscriptionRequest request) {
    	logger.info("OnticInfoController::stopSubscription Start");
    	
    	request.setSubsID(subsid);
    	
    	logger.info("StopSubscriptionRequest values {}", request);
    	
    	rabbitAdapter.sendReceivedMessage(new ReceivedMessage(request.toString()));
    	
    	onticInfoService.stopSubscription(request);
    	
    	logger.info("OnticInfoController::stopSubscription End");
    	
    	rabbitAdapter.sendSentMessage(new SentMessage(HttpStatus.NO_CONTENT.toString()));
    }
    
    @RequestMapping(value = ControllerActions.MODIFY_DELIVERY_CONDITIONS_ACTION, method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void modifyDegredationReportDeliveryConditions(@PathVariable String sessionid, @RequestBody ModifyDeliveryConditionsRequest request) {
    	logger.info("OnticInfoController::modifyDegredationReportDeliveryConditions Start");
    	
    	request.setSessionID(sessionid);
    	
    	logger.info("ModifyDeliveryConditionsRequest values : {}", request);
    	
    	rabbitAdapter.sendReceivedMessage(new ReceivedMessage(request.toString()));
    	
    	onticInfoService.modifyDegredationReportDeliveryConditions(request);
    	
    	logger.info("OnticInfoController::modifyDegredationReportDeliveryConditions End");
    	
    	rabbitAdapter.sendSentMessage(new SentMessage(HttpStatus.OK.toString()));
    }
    
    @RequestMapping(value = ControllerActions.STOP_DELIVERY_CONDITIONS_ACTION, method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void stopDegredationReportDeliveryConditions(@PathVariable String sessionid, @RequestBody StopDeliveryConditionsRequest request) {
    	logger.info("OnticInfoController::stopDegredationReportDeliveryConditions Start");
    	
    	logger.info("StopDeliveryConditionsRequest values : {}", request);
    	
    	rabbitAdapter.sendReceivedMessage(new ReceivedMessage(request.toString()));
    	
    	request.setSessionID(sessionid);
    	
    	onticInfoService.stopDegredationReportDeliveryConditions(request);
    	
    	logger.info("OnticInfoController::stopDegredationReportDeliveryConditions End");
    	
    	rabbitAdapter.sendSentMessage(new SentMessage(HttpStatus.NO_CONTENT.toString()));
    }
}
