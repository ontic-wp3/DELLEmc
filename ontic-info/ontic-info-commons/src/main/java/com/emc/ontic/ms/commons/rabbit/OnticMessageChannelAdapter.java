package com.emc.ontic.ms.commons.rabbit;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class OnticMessageChannelAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger(OnticMessageChannelAdapter.class);
	
	private int rabbitmq_port = 5672;
	private String rabbitmq_server = "";
	private String rabbitmq_virtualhost = "vh_ontic";
	private String rabbitmq_username = "ontic";
	private String rabbitmq_password = "changeme";
	private String rabbitmq_exchange = "ex_ontic";
	private String rabbitmq_pgf_sent_routingkey = "pgf_sent";
	private String rabbitmq_pgf_received_routingkey = "pgf_received";
	
	private RabbitTemplate rabbitTemplate = null;
	
	@Autowired
	Environment environment;
	
	private String getEnvironmentValue(String propertiesKey, String environmentKey){
		String value = environment.getProperty(propertiesKey);
		if(environment.getProperty(environmentKey) != null){
			value = environment.getProperty(environmentKey); 
		}
		return value;
	}
		
	private synchronized RabbitTemplate getRabbitTemplate(){
		if(rabbitTemplate == null){
			rabbitmq_server = getEnvironmentValue("rabbitmq.server", "RABBITMQ_SERVER");
			rabbitmq_port = new Integer(getEnvironmentValue("rabbitmq.port", "RABBITMQ_PORT")).intValue();
			rabbitmq_virtualhost = getEnvironmentValue("rabbitmq.virtualhost", "RABBITMQ_VIRTUAL_HOST");
			rabbitmq_username = getEnvironmentValue("rabbitmq.username", "RABBITMQ_USERNAME");
			rabbitmq_password = getEnvironmentValue("rabbitmq.password", "RABBITMQ_PASSWORD");
			rabbitmq_exchange = getEnvironmentValue("rabbitmq.exchange", "RABBITMQ_EXCHANGE");
			rabbitmq_pgf_sent_routingkey = getEnvironmentValue("rabbitmq.pgf.sent.routingkey", "RABBITMQ_PGF_SENT_ROUTING_KEY");
			rabbitmq_pgf_received_routingkey = getEnvironmentValue("rabbitmq.pgf.received.routingkey", "RABBITMQ_PGF_RECEIVED_ROUTING_KEY");
			
			CachingConnectionFactory connectionFactory = new CachingConnectionFactory(rabbitmq_server);
	        connectionFactory.setUsername(rabbitmq_username);
	        connectionFactory.setPassword(rabbitmq_password);
	        connectionFactory.setPort(rabbitmq_port);
	        connectionFactory.setVirtualHost(rabbitmq_virtualhost);
	        rabbitTemplate = new RabbitTemplate(connectionFactory);
		}
		return rabbitTemplate;
	}
	
	public void sendReceivedMessage(AbstractMessage message){
		send(rabbitmq_pgf_received_routingkey, message);
	}
	
	public void sendSentMessage(AbstractMessage message){
		send(rabbitmq_pgf_sent_routingkey, message);
	}
	
	private void send(String routingKey, AbstractMessage message){
		String json;
		try {
			json = new ObjectMapper().writeValueAsString(message);
			getRabbitTemplate().convertAndSend(rabbitmq_exchange, routingKey, json.getBytes());
		} catch (JsonGenerationException e) {
			logger.error("Error serializing the message. " + e.toString());
		} catch (JsonMappingException e) {
			logger.error("Error serializing the message. " + e.toString());
		} catch (IOException e) {
			logger.error("Error serializing the message. " + e.toString());
		}
	}
}
