package com.emc.ontic.ms.commons.rabbit;

public class SentMessage extends AbstractMessage{
	
	public SentMessage(String value){
		super(value);
	}
}
