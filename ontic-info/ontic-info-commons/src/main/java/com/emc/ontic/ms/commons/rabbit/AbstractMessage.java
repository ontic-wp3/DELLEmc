package com.emc.ontic.ms.commons.rabbit;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class AbstractMessage {
	
	private String timestamp = "";
	
	private String notification = ""; 
	
	public AbstractMessage(){
		this("");
	}
	
	public AbstractMessage(String value){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		timestamp = format.format(new Date());
		this.notification = value;
	}
	
	public String getNotification(){
		return notification;
	}
	
	public void setNotification(String value){
		this.notification = value;
	}
	
	public String getTimestamp(){
		return this.timestamp;
	}
	
	public void setTimestamp(String value){
		this.timestamp = value;
	}
}
