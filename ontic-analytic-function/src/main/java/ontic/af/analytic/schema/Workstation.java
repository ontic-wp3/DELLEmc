package ontic.af.analytic.schema;

public class Workstation {
	
	private int good = 0;
	
	private int bad = 0;
	
	private int medium = 0;
	
	private int green = 0;
	
	private int orange = 0;
	
	private int red = 0;
	
	private String ip = null;
	
	private String segmentation = null;
		
	private int total = 0;
	
	public void setSegmentation(String value){
		this.segmentation = value;
	}
	
	public String getSegmentation(){
		return this.segmentation;
	}
	
	public void setMedium(int value){
		this.medium = value;
	}
	
	public int getMedium(){
		return this.medium;
	}
	
	public void setBad(int value){
		this.bad = value;
	}
	
	public int getBad(){
		return this.bad;
	}
	
	public void setGood(int value){
		this.good = value;
	}
	
	public int getGood(){
		return this.good;
	}
	
	public void setTotal(int value){
		this.total = value;
	}
	
	public int getTotal(){
		return this.total;
	}
	
	public void setGreen(int value){
		this.green = value;
	}
	
	public int getGreen(){
		return this.green;
	}
	
	public void setRed (int value){
		this.red = value;
	}
	
	public int getRed(){
		return this.red;
	}
	
	public void setOrange(int value){
		this.orange = value;
	}
	
	public int getOrange(){
		return this.orange;
	}
	
	public void setIp(String value){
		this.ip = value;
	}
	
	public String getIp(){
		return this.ip;
	}
}
