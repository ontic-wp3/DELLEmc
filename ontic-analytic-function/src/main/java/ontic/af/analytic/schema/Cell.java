package ontic.af.analytic.schema;

import java.util.ArrayList;
import java.util.List;

public class Cell {
	
	private int weighted_green = 0;
	
	private int weighted_orange = 0;
	
	private int weighted_red = 0;
	
	private int weighted_total = 0;
	
	private int weighted_bad = 0;
	
	private int weighted_good = 0;
	
	private int weighted_medium = 0;
	
	private int total = 0;
	
	private int bad = 0;
	
	private int good = 0;
	
	private int medium = 0;
	
	private String name;
	
	private String id;
	
	private String timestamp = null;
	
	public int getTotal(){
		return this.total;
	}
	
	public void setTotal(int value){
		this.total = value;
	}
	
	public int getBad(){
		return this.bad;
	}
	
	public void setBad(int value){
		this.bad = value;
	}
	
	public int getGood(){
		return this.good;
	}
	
	public void setGood(int value){
		this.good = value;
	}
	
	public int getMedium(){
		return this.medium;
	}
	
	public void setMedium(int value){
		this.medium = value;
	}
	
	public int getWeighted_Medium(){
		return this.weighted_medium;
	}
	
	public void setWeighted_Medium(int value){
		this.weighted_medium = value;
	}
	
	public int getWeighted_Good(){
		return this.weighted_good;
	}
	
	public void setWeighted_Good(int value){
		this.weighted_good = value;
	}
	
	public int getWeighted_Bad(){
		return this.weighted_bad;
	}
	
	public void setWeighted_Bad(int value){
		this.weighted_bad = value;
	}
	
	public int getWeighted_Total(){
		return this.weighted_total;
	}
	
	public void setWeighted_Total(int value){
		this.weighted_total = value;
	}
	
	public void setId(String value){
		this.id = value;
	}
	
	public String getId(){
		return this.id;
	}
	
	public void setTimestamp(String value){
		this.timestamp = value;
	}
	
	public String getTimestamp(){
		return this.timestamp;
	}
	
	public void setName(String value){
		this.name = value;
	}
	
	public String getName(){
		return this.name;
	}
	
	private List<Workstation> workstations = new ArrayList<Workstation>();
	
	public List<Workstation> getWorkstations(){
		return this.workstations;
	}
	
	public void setWorkstations(List<Workstation> value){
		this.workstations = value;
	}
	
	public int getWeighted_Green(){
		return this.weighted_green;
	}
	
	public void setWeighted_Green(int value){
		this.weighted_green = value;
	}
	
	public void setWeighted_Red (int value){
		this.weighted_red= value;
	}
	
	public int getWeighted_Red(){
		return this.weighted_red;
	}
	
	public void setWeighted_Orange(int value){
		this.weighted_orange = value;
	}
	
	public int getWeighted_Orange(){
		return this.weighted_orange;
	}
}
