package com.emc.ontic.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class App {
	
	public static void main(String [] args) throws FileNotFoundException, IOException{
		BufferedWriter bw = null;
		
		String file_to_train = "C:\\_CloudStation\\Pivotal\\Customers\\ONTIC\\Use Case Implementation\\Latest_Logs\\to_train.txt";
		String file_to_train_clean = "C:\\_CloudStation\\Pivotal\\Customers\\ONTIC\\Use Case Implementation\\Latest_Logs\\to_train_clean.txt";
					
		try(BufferedReader br = new BufferedReader(new FileReader(file_to_train))) {
            
            File fout = new File(file_to_train_clean);
        	FileOutputStream fos = new FileOutputStream(fout);
         
        	bw = new BufferedWriter(new OutputStreamWriter(fos));
                     
		    String line = br.readLine();
		    while (line != null) {
		    	StringTokenizer tokenizer = new StringTokenizer(line, " ");
		    	if(tokenizer.countTokens() != 139){
		    		System.out.println("Linea sin 139 campos");
		    	}else{
		    		 bw.write(line);
		    		 bw.newLine();
		    	}
		    	line = br.readLine();
		    }
		} finally {
            try {
                // Close the writer regardless of what happens...
            	bw.flush();
                bw.close();
            } catch (Exception e) {
            }
        }
		
	}
}
