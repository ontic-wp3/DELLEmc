package com.emc.ontic.network;

import java.io.IOException;
import java.util.Arrays;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;

public class ActuatorImpl implements Actuator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ActuatorImpl.class);
	
	private static final String WONDERSHAPPER = "wondershaper";
	
	private static final String SUDO = "sudo";
	
	private static final String CLEAR = "clear";
	
	private String network = "eth0";
	
	private static final String UPLOAD_TOKEN = "upload_speed";
	
	private static final String DOWNLOAD_TOKEN = "download_speed";
	
	public void setNetwork(String value){
		this.network = value;
	}
	
	public String getNetwork(){
		return this.network;
	}

	public void write(Message<?> message) throws IOException {
		try{
			String value = null;
			if (message.getPayload() instanceof String){
				value = new String((String)message.getPayload());
			}else{
				value = new String((byte[])message.getPayload());
			}
			JSONObject obj = new JSONObject(value);
			int upload = obj.getInt(UPLOAD_TOKEN);
			int download = obj.getInt(DOWNLOAD_TOKEN);
			String [] command = null;
			if( (upload != -1) && (download != -1)){
				command = new String[5];
				command[2] = getNetwork();
				command[3] = new Integer(upload).toString();
				command[4] = new Integer(download).toString();
			} else {
				command = new String[4];
				command[2] = CLEAR;
				command[3] = getNetwork();
				
			}
			command[0] = SUDO;
			command[1] = WONDERSHAPPER;
						
			Process process = Runtime.getRuntime().exec(command);

	        // Esperar a que termine el proceso y obtener su valor de salida
	        process.waitFor();
	        int result = process.exitValue();
	        if (result != 0) {
	        	LOGGER.error("Exit value " + result);
	        } else{
	        	LOGGER.info("Executed the command " + Arrays.toString(command));
	        }
		}catch (Exception e){
			LOGGER.error("Error processing message. " + e.toString());
		}
	}
}
