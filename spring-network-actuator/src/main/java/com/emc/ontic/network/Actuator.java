package com.emc.ontic.network;

import java.io.IOException;

import org.springframework.messaging.Message;

public interface Actuator {
	void write (Message<?> message) throws IOException;
}
