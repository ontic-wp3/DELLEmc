package com.emc.ontic.network;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@Configuration
@ImportResource("classpath:/actuator-context.xml")
@EnableAutoConfiguration
public class ActuatorServer implements CommandLineRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(ActuatorServer.class);
    
	@Autowired
    private ApplicationContext ctx;
	
	public static void main(final String... args) {
		SpringApplication app = new SpringApplication(ActuatorServer.class);
		app.setWebEnvironment(false);
		app.run(args);
	}

	public void run(String... args) throws Exception {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("\n========================================================="
					  + "\n                                                         "
					  + "\n          Starting a Actuator Server node                "
					  + "\n                                                         "
					  + "\n=========================================================" );
		}
	}
}