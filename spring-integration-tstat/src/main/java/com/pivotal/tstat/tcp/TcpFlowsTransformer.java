package com.pivotal.tstat.tcp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import com.pivotal.tstat.classification.db.DBManagement;
import com.pivotal.tstat.ingester.MessageTransformer;

public class TcpFlowsTransformer implements MessageTransformer{
	
	private static final Log LOGGER = LogFactory.getLog(TcpFlowsTransformer.class);
	
	private boolean supressCommentLines = false;
	
	private String networkInterface = null;
	
	private String segmentation = null;
	
	private boolean savedSegmentation = false;
	
	private DBManagement dbManagement;
	
	public void setDbManagement(DBManagement value){
		this.dbManagement = value;
	}
	
	public DBManagement getDbManagement(){
		return this.dbManagement;
	}
	
	public void setSegmentation (String value){
		this.segmentation = value;
	}
	
	public String getSegmentation(){
		return this.segmentation;
	}
		
	public void setNetworkInterface (String value){
		this.networkInterface = value;
	}
	
	public String getNetworkInterface(){
		return this.networkInterface;
	}
	
	public boolean isSupressCommentLines() {
		return supressCommentLines;
	}
	
	public void setSupressCommentLines(boolean supressCommentLines) {
		this.supressCommentLines = supressCommentLines;
	}
	
	private synchronized void updateSegmentation(String ip){
		if (!savedSegmentation){
			dbManagement.updateLocation(getSegmentation(), ip);
			savedSegmentation = true;
		}
	}
	
	private InetAddress getCurrentIp(String networkInterface){
		try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            boolean checkInterface;
            while (networkInterfaces.hasMoreElements()) {
            	checkInterface = false;
                NetworkInterface ni = (NetworkInterface) networkInterfaces.nextElement();
                if (networkInterface != null){
                	checkInterface = (networkInterface.equals(ni.getName()));
                }else{
                	checkInterface = true;
                }
                if(checkInterface){
                	LOGGER.info("Cheking network interface " + ni.getName());
	                Enumeration<InetAddress> nias = ni.getInetAddresses();
	                while(nias.hasMoreElements()) {
	                    InetAddress ia= (InetAddress) nias.nextElement();
	                    if (!ia.isLinkLocalAddress() 
	                     && !ia.isLoopbackAddress()
	                     && ia instanceof Inet4Address) {
	                        return ia;
	                    }
	                }
                }
            }
        } catch (SocketException e) {
        	LOGGER.error("unable to get current IP " + e.getMessage());
        }
        return null;
	}
	
	private InetAddress getCurrentIp() {
		InetAddress inetAddress = null;
        if (getNetworkInterface() != null){
        	LOGGER.info("Getting IP for network interface " + getNetworkInterface());
        	inetAddress = getCurrentIp(getNetworkInterface());
        	if (inetAddress == null){
        		LOGGER.info("Getting IP for the first network interface with a InetAddress that is not local o lookback.");
        		inetAddress = getCurrentIp(null);
        		if(inetAddress != null){
        			LOGGER.info("Found IP " + inetAddress.toString());
        		}
        	}else{
        		LOGGER.info("Found IP " + inetAddress.toString() + " for network interface " + getNetworkInterface());
        	}
        }else{
        	LOGGER.info("Getting IP for the first network interface with a InetAddress that is not local o lookback.");
        	inetAddress = getCurrentIp(null);
        	if(inetAddress != null){
    			LOGGER.info("Found IP " + inetAddress.toString());
    		}
        }
        return inetAddress;
    }

	public Message<byte[]> transform(Message<?> message) throws TstatException {
				
		if (message.getPayload() instanceof File){
			// Open the file
			FileInputStream fs = null; 
			InputStreamReader is = null;
			BufferedReader br = null;
			File file= (File)message.getPayload();
			
			LOGGER.info("Analyzing the file: " + file.getAbsolutePath());

			try{
				fs = new FileInputStream(file);
				is = new InputStreamReader(fs);
				br = new BufferedReader(is);
				String strLine;
				StringBuilder sb = new StringBuilder();
				
				//Read File Line By Line
				while ((strLine = br.readLine()) != null)  
				{
					 if(!(strLine.startsWith("#") && supressCommentLines))
					 {					 
						 sb.append(strLine);
						 sb.append(System.lineSeparator());
					 }
				}
				
				File parent_file= file.getParentFile();
				
				String parent_name=(parent_file.getName());
				parent_name=parent_name.substring(0,parent_name.indexOf(".out"));
			    
				String ip =getCurrentIp().toString();
				if (ip.startsWith("/"))
				{
					ip=ip.substring(1);
				}
				updateSegmentation(ip);
				
				LOGGER.info("Creating the message with headers TSTAT_TIME" + parent_name + " and TSTAT_IP " + ip);
			
				Message<byte[]> message1= MessageBuilder.withPayload(sb.toString().getBytes())
				.setHeader("TSTAT_TIME", parent_name)
				.setHeader("TSTAT_IP", ip)
				.build();
               
              	return message1;
				
			}catch(IOException e){
				LOGGER.error("Error reading the file " + file.getAbsolutePath() + ". Error: " + e.toString() );
				throw new TstatException("Error reading the file");
			}finally{
				if (br != null) { try { br.close(); } catch(IOException t) { } }
				if (is != null) { try { is.close(); } catch(IOException t) { } }
				if (fs != null) { try { fs.close(); } catch(IOException t) { } }
			}
		}
		else{
			LOGGER.error("Invalid message format, expected payload type of File");
			throw new TstatException("Invalid message format, expected payload type of File");
		}
	}
}
