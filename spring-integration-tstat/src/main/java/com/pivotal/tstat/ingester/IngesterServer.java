package com.pivotal.tstat.ingester;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:ingester-tstat-context.xml")
@ComponentScan
public class IngesterServer implements CommandLineRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(IngesterServer.class);
    
	/**
	 * Load the Spring Integration Application Context
	 *
	 * @param args - command line arguments
	 */
	public static void main(final String... args) {
		SpringApplication app = new SpringApplication(IngesterServer.class);
		app.setWebEnvironment(false);
		app.run(args);
	}

	public void run(String... args) throws Exception {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("\n========================================================="
					  + "\n                                                         "
					  + "\n          Starting a tstat ingester node-                "
					  + "\n                                                         "
					  + "\n=========================================================" );
		}
	}
}