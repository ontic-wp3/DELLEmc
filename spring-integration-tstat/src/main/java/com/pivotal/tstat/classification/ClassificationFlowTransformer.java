package com.pivotal.tstat.classification;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;

import com.pivotal.tstat.classification.db.DBManagement;
import com.pivotal.tstat.classification.db.entity.Location;
import com.pivotal.tstat.ingester.MessageTransformer;
import com.pivotal.tstat.tcp.TstatException;

public class ClassificationFlowTransformer implements MessageTransformer{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ClassificationFlowTransformer.class);

    private Classifier classifier=null;
    
    private DBManagement dbManagement;
	
	public void setDbManagement(DBManagement value){
		this.dbManagement = value;
	}
	
	public DBManagement getDbManagement(){
		return this.dbManagement;
	}

	public Classifier getClassifier() {
		return classifier;
	}

	public void setClassifier(Classifier classifier) {
		this.classifier = classifier;
	}

	public Message<byte[]> transform(Message<?> message) throws TstatException {

		String mensaje=new String((byte[])message.getPayload());
		MessageHeaders headers= message.getHeaders();
		String ip = (String)headers.get("TSTAT_IP");
		String timestamp = (String)headers.get("TSTAT_TIME");
		int total=0;
		double good=0;
		double medium=0;
		double bad=0;
		
		LOGGER.info("Flow from IP: " + ip + " and Tstat time: " + timestamp);
			
		if (mensaje.length() > 10){
    	    String[] flows = mensaje.split("\\n");
    	    
    	    for (String flow : flows){
    	    	 LOGGER.debug("Flow:" + flow);
	    	     Classification classification = classifier.predict(flow);
	    	     
	    	     // No exception
	    	     if (classification.getException().length() == 0){
	    	    	 LOGGER.debug("Classification:" + classification.getPrediction());
		    	     
		    	     if ((classification.getPrediction().compareTo("good")) == 0){
		    	    	 good++;
		    	     }else if (classification.getPrediction().compareTo("bad") == 0){
		    	    	 bad++;
		    	     }else{
		    	    	 medium++;
		    	     }
		    	     total++;
	    	     }else{
	    	    	 LOGGER.error("Exception: " + classification.getException() + ", predicting the flow: " + flow);
	    	     }
    	    }
		}
		
		Workstation workstation = new Workstation();
		workstation.setBad((int)bad);
		workstation.setGood((int)good);
		workstation.setMedium((int)medium);
		workstation.setTotal(total);
		workstation.setIp(ip);
		workstation.setOrange((int) Math.round((medium/(double)total)*100));
		workstation.setRed((int) Math.round((bad/(double)total)*100));
		workstation.setGreen((int) Math.round((good/(double)total)*100));
		
		Cell cell = new Cell();
		
		Location location = dbManagement.getLocationByIp(ip);
		String cellName = "unknown";
		String cellId = "-1";
		if (location != null){
			cellName = location.getCellName();
			cellId = location.getCellId();
			workstation.setSegmentation(location.getSegmentationId());
		}
		cell.setName(cellName);
		cell.setId(cellId);
		cell.setTimestamp(timestamp);
		cell.getWorkstations().add(workstation);
		
		ObjectMapper mapper = new ObjectMapper();
		String salida = "";
		try {
			salida = mapper.writeValueAsString(cell);
		} catch (JsonGenerationException e) {
			LOGGER.error("Error serializing the object to json. " + e.toString());
		} catch (JsonMappingException e) {
			LOGGER.error("Error serializing the object to json. " + e.toString());
		} catch (IOException e) {
			LOGGER.error("Error serializing the object to json. " + e.toString());
		}
		
		/*
		String salida;
		if(total !=0)
		{
			salida="{\"workstations\":[{\"ip\":\""+ip+"\",\"flows\":"+total+",\"red\":"+(int) Math.round((bad/(double)total)*100)+",\"green\":"+(int) Math.round((good/(double)total)*100)+",\"orange\":"+(int) Math.round((medium/(double)total)*100)+"}]}";
		}else
		{
			salida="{\"workstations\":[{\"ip\":\""+ip+"\",\"flows\":"+total+",\"red\":"+0+",\"green\":"+0+",\"orange\":"+0+"}]}";
			
		}
		*/
		
	   LOGGER.debug(salida);
 	   Message<byte[]> message1= MessageBuilder.withPayload(salida.getBytes())
 			  .setHeader("CLASSIFICATION_TIME", System.currentTimeMillis() )
 			  .setHeader("TSTAT_TIME", timestamp )
 			  .setHeader("CLASSIFICAITON_CELL",  timestamp + " " + cellName)
 			  .build();
 	   
 	  return message1;
	}
}
