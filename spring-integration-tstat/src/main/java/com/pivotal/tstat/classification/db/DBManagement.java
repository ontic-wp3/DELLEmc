package com.pivotal.tstat.classification.db;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.pivotal.tstat.classification.db.entity.Location;
import com.pivotal.tstat.classification.db.repositories.LocationRepository;

public class DBManagement {
	
	@Autowired
	private LocationRepository locationRepo;
	
	@Transactional
	public HashMap<String, Location> getCellIps(){
		HashMap<String, Location> hash = new HashMap<String,Location>();
		for (Location loc : locationRepo.findAll()){
			if ((loc.getIp() != null) && (!(hash.containsKey(loc.getIp())))){
				hash.put(loc.getIp(), loc);
			}
		}
		return hash;
	}
	
	@Transactional
	public Location getLocationByIp(String ip){
		Location result = null;
		for (Location loc : locationRepo.findAll()){
			if (loc.getIp() != null && ip.equals(loc.getIp())){
				result = loc;
				break;
			}
		}
		return result;
	}
	
	@Transactional
	public HashMap<String, Location> getLocationsByCellId(String cellId){
		HashMap<String, Location> hash = new HashMap<String,Location>();
		for (Location loc : locationRepo.findAll()){
			if (loc.getCellId() != null && cellId.equals(loc.getCellId())){
				hash.put(loc.getSegmentationId(), loc);
			}
		}
		return hash;
	}
	
	@Transactional
	public void updateLocation(String segmentationId, String ip){
		for (Location loc : locationRepo.findAll()){
			if (segmentationId != null && segmentationId.equals(loc.getSegmentationId())){
				loc.setIp(ip);
				locationRepo.save(loc);
				break;
			}
		}
	}
}
