package com.pivotal.tstat.classification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.messaging.Message;

public class MessageAggregator {
 
	private static final Log LOGGER = LogFactory.getLog(MessageAggregator.class);
	
	public Cell addMessages(List<Message<?>> messages) {
		Cell result = null;
		ObjectMapper mapper = new ObjectMapper();
		
		for (Message<?> message : messages) {
			try {
				Cell cell = mapper.readValue(new String((byte[])message.getPayload()), Cell.class);
				if (result == null){
					result = cell; 
				}else{
					result.getWorkstations().addAll(cell.getWorkstations());
				}
			} catch (JsonParseException e) {
				LOGGER.error("Error deserializing the json. " + e.toString());
			} catch (JsonMappingException e) {
				LOGGER.error("Error deserializing the json. " + e.toString());
			} catch (IOException e) {
				LOGGER.error("Error deserializing the json. " + e.toString());
			}
		}
		
		return result;
	}
}