package com.pivotal.tstat.classification;

import java.io.IOException;
import java.util.HashMap;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import com.pivotal.tstat.classification.db.DBManagement;
import com.pivotal.tstat.classification.db.entity.Location;

public class ClassificationGroupTransformer {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ClassificationGroupTransformer.class);
	
	private DBManagement dbManagement;
		
	public void setDbManagement(DBManagement value){
		this.dbManagement = value;
	}
	
	public DBManagement getDbManagement(){
		return this.dbManagement;
	}
	
	public Message<byte[]> transform(Message<?> message){
	
		Cell cell = (Cell)message.getPayload();
		
		HashMap<String, Location> hash = getDbManagement().getLocationsByCellId(cell.getId());
		
		// Weight flows
		double weighted_good =0;
		double weighted_medium =0;
		double weighted_bad =0;
		int weighted_total = 0;
		double good =0;
		double medium =0;
		double bad =0;
		int total = 0;
		
		for (Workstation workstation : cell.getWorkstations()){
			Location loc = hash.get(workstation.getSegmentation());
			weighted_good = weighted_good + (workstation.getGood() * loc.getWeight());
			good = good + workstation.getGood();
			weighted_bad = weighted_bad + (workstation.getBad() * loc.getWeight());
			bad = bad + workstation.getBad();
			weighted_medium = weighted_medium + (workstation.getMedium() * loc.getWeight());
			medium = medium + workstation.getMedium();
		}
		
		weighted_total = (int)weighted_good + (int)weighted_bad + (int)weighted_medium;
		total = (int)good + (int)bad + (int)medium;
		
		cell.setWeighted_Orange((int) Math.round((weighted_medium/(double)weighted_total)*100));
		cell.setWeighted_Red((int) Math.round((weighted_bad/(double)weighted_total)*100));
		cell.setWeighted_Green((int) Math.round((weighted_good/(double)weighted_total)*100));
		cell.setWeighted_Bad((int)weighted_bad);
		cell.setWeighted_Good((int)weighted_good);
		cell.setWeighted_Medium((int)weighted_medium);
		cell.setWeighted_Total(weighted_total);
		cell.setBad((int)bad);
		cell.setGood((int)good);
		cell.setMedium((int)medium);
		cell.setTotal((int)total);
		
		ObjectMapper mapper = new ObjectMapper();
		String salida = "";
		try {
			salida = mapper.writeValueAsString(cell);
		} catch (JsonGenerationException e) {
			LOGGER.error("Error serializing the object to json. " + e.toString());
		} catch (JsonMappingException e) {
			LOGGER.error("Error serializing the object to json. " + e.toString());
		} catch (IOException e) {
			LOGGER.error("Error serializing the object to json. " + e.toString());
		}
		
		LOGGER.debug("Weighted flows json: " + salida);
		Message<byte[]> message1= MessageBuilder.withPayload(salida.getBytes())
					.setHeader("TSTAT_TIME", cell.getTimestamp() )
					.setHeader("CLASSIFICATION_TIME", System.currentTimeMillis() )
					.build();
	 	   
		return message1;
	}
}
