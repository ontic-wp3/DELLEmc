package com.pivotal.tstat.classification.db.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pivotal.tstat.classification.db.entity.Location;

@Repository
public interface LocationRepository extends CrudRepository<Location, String> {
}
