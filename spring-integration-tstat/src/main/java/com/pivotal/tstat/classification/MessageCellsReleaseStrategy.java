package com.pivotal.tstat.classification;

import org.springframework.integration.aggregator.ReleaseStrategy;
import org.springframework.integration.store.MessageGroup;

import com.pivotal.tstat.classification.db.DBManagement;

public class MessageCellsReleaseStrategy implements ReleaseStrategy{
	
	private DBManagement dbManagement;
	
	public void setDbManagement(DBManagement value){
		this.dbManagement = value;
	}
	
	public DBManagement getDbManagement(){
		return this.dbManagement;
	}
	
	private synchronized int getGroupSize(){
		return dbManagement.getCellIps().keySet().size();
	}

	public boolean canRelease(MessageGroup arg0) {
		if (arg0.size() >= getGroupSize()){
			return true;	
		}else{
			return false;
		}
	}
}
