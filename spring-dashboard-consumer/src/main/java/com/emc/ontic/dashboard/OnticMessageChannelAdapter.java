package com.emc.ontic.dashboard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class OnticMessageChannelAdapter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OnticMessageChannelAdapter.class);
	
	private int rabbitmq_port = 5672;
	private String rabbitmq_server = "";
	private String rabbitmq_virtualhost = "vh_ontic";
	private String rabbitmq_username = "ontic";
	private String rabbitmq_password = "changeme";
	private String rabbitmq_exchange = "ex_ontic";
	
	private RabbitTemplate rabbitTemplate = null;
	
	@Autowired
	Environment environment;
	
	private String getEnvironmentValue(String propertiesKey, String environmentKey){
		String value = environment.getProperty(propertiesKey);
		if(environment.getProperty(environmentKey) != null){
			value = environment.getProperty(environmentKey); 
		}
		return value;
	}
		
	private synchronized RabbitTemplate getRabbitTemplate(){
		if(rabbitTemplate == null){
			rabbitmq_server = getEnvironmentValue("rabbitmq.server", "RABBITMQ_SERVER");
			rabbitmq_port = new Integer(getEnvironmentValue("rabbitmq.port", "RABBITMQ_PORT")).intValue();
			rabbitmq_virtualhost = getEnvironmentValue("rabbitmq.virtualhost", "RABBITMQ_VIRTUAL_HOST");
			rabbitmq_username = getEnvironmentValue("rabbitmq.username", "RABBITMQ_USERNAME");
			rabbitmq_password = getEnvironmentValue("rabbitmq.password", "RABBITMQ_PASSWORD");
			rabbitmq_exchange = getEnvironmentValue("rabbitmq.exchange", "RABBITMQ_EXCHANGE");
			
			CachingConnectionFactory connectionFactory = new CachingConnectionFactory(rabbitmq_server);
	        connectionFactory.setUsername(rabbitmq_username);
	        connectionFactory.setPassword(rabbitmq_password);
	        connectionFactory.setPort(rabbitmq_port);
	        connectionFactory.setVirtualHost(rabbitmq_virtualhost);
	        rabbitTemplate = new RabbitTemplate(connectionFactory);
		}
		return rabbitTemplate;
	}
	
	public void send(RequestActuator request){
		LOGGER.info("Trying to write message: " + request.getMessageToQueue() + " with routing key " + request.getRouting_queue());
		getRabbitTemplate().convertAndSend(rabbitmq_exchange, request.getRouting_queue(), request.getMessageToQueue().getBytes());
	}
}
