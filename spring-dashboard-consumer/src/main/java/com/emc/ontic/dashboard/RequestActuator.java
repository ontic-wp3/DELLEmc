package com.emc.ontic.dashboard;

public class RequestActuator {
	
	private int upload_speed;
	private int download_speed;
	private String routing_queue;

	public int getUpload_speed() {
		return upload_speed;
	}


	public void setUpload_speed(int value) {
		this.upload_speed = value;
	}
	
	public String getRouting_queue() {
		return routing_queue;
	}
	
	public void setRouting_queue(String value) {
		this.routing_queue = value;
	}

	public int getDownload_speed() {
		return download_speed;
	}


	public void setDownload_speed(int value) {
		this.download_speed = value;
	}
	
	public String getMessageToQueue(){
		return "{\"upload_speed\":" + getUpload_speed() + ",\"download_speed\":" + getDownload_speed() + "}";
	}
	
	public String toString(){
		return "{\"upload_speed\":" + getUpload_speed() + ",\"download_speed\":" + getDownload_speed() + ",\"routing_queue\":\"" + getRouting_queue() + "\"}";
	}
}
