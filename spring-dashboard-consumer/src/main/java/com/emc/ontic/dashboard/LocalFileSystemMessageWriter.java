package com.emc.ontic.dashboard;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;

public class LocalFileSystemMessageWriter extends AbstractWriter{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LocalFileSystemMessageWriter.class);
	
	public void writeFirst(Message<?> message) throws IOException {
		try{
			if (message.getPayload() instanceof String){
				writeToFile((String)message.getPayload(), true);
			}else{
				writeToFile(new String((byte[])message.getPayload()), true);	
			}
			
		}catch (Exception e){
			LOGGER.error("Error processing the message. " + e.toString());
		}
	}

	public void writeLast(Message<?> message) throws IOException {
		try{
			if (message.getPayload() instanceof String){
				writeToFile((String)message.getPayload(), false);
			}else{
				writeToFile(new String((byte[])message.getPayload()), false);	
			}
			
		}catch (Exception e){
			LOGGER.error("Error processing the message. " + e.toString());
		}
	}
}
