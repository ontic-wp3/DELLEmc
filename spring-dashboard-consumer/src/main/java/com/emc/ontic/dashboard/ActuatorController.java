package com.emc.ontic.dashboard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ActuatorController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private OnticMessageChannelAdapter messageAdapter;
	
	@RequestMapping(value = "/network", method = RequestMethod.POST)
    public ResponseEntity<?> activation(@RequestBody RequestActuator requestActuator, HttpServletRequest request) {
    	LOGGER.info("Writing message " + requestActuator.toString() + " to rabbitMQ ... ");
		try{
			messageAdapter.send(requestActuator);
		}catch (Exception e){
			LOGGER.error("Error writing to the queue." + e.toString());
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
		return new ResponseEntity<>(HttpStatus.OK);//200
    }
	
	@RequestMapping(value = "/network/{upload}/{download}/{routing}", method = RequestMethod.GET)
	public ResponseEntity<?> activation(@PathVariable int upload, @PathVariable int download, @PathVariable String routing){ 
		RequestActuator requestActuator = new RequestActuator();
		requestActuator.setDownload_speed(download);
		requestActuator.setUpload_speed(upload);
		requestActuator.setRouting_queue(routing);
		LOGGER.info("Writing message " + requestActuator.toString() + " to rabbitMQ ... ");
		try{
			messageAdapter.send(requestActuator);
		}catch (Exception e){
			LOGGER.error("Error writing to the queue." + e.toString());
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
		return new ResponseEntity<>(HttpStatus.OK);//200
    }
}