package com.emc.ontic.dashboard;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractWriter implements Writer{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractWriter.class);
	
	private static final int SIZE = 10;
	
	private String _file = null;
	
	public void setFile(String value)
	{
		_file = value;
	}
	
	public String getFile()
	{
		return _file;
	}
	
	public void writeToFile(String message, boolean direction) throws IOException{
		FileWriter file = null;

		try {
			LOGGER.debug("Analyzing the json message: " + message);
			
			JSONObject obj = new JSONObject(new String(message));
		
			JSONParser parser = new JSONParser();
			JSONArray array = (JSONArray) parser.parse(new FileReader(getFile()));
			
			while(array.size() >= SIZE){
				if (direction){
					array.remove(array.size() - 1);	
				}else{
					array.remove(array.size() - SIZE);
				}
				
			}
			if (direction){
				array.add(0, obj);	
			}else{
				array.add(obj);
			}
			
			file = new FileWriter(getFile());
			
			file.write(array.toJSONString());
		} catch (JSONException e) {
			LOGGER.error("Error reading the json ... " + e.toString());
		} catch (ParseException e) {
			LOGGER.error("Error parsing the json file (array)... " + e.toString());
		} catch (Exception e){
			LOGGER.error("Generic error processing the message." + e.toString());
		}
		finally{
			if(file != null){
				file.close();
				LOGGER.debug("Analyzed the json message: " + message);
			}
		}
	}
}