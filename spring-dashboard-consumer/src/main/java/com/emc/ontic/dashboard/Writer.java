package com.emc.ontic.dashboard;

import java.io.IOException;

import org.springframework.messaging.Message;

public interface Writer {

	void writeFirst (Message<?> message) throws IOException;
	
	void writeLast (Message<?> message) throws IOException;
}
